package jp.co.esco.peppercampaign.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

@SuppressLint("AppCompatCustomView")
public class OnOffButton extends ImageButton {

    public OnOffButton(Context context) {
        super(context);
    }

    public OnOffButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OnOffButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public OnOffButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void changeActiveState(boolean isActive) {
        if (isActive) {
            this.setAlpha(1.0f);
        } else {
            this.setAlpha(0.6f);
        }
    }

    public void changeState(boolean isOn) {
        this.setEnabled(isOn);
        this.changeActiveState(isOn);
    }
}
