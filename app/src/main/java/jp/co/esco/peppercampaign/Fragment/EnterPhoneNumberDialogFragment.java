package jp.co.esco.peppercampaign.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.View.OnOffButton;

public class EnterPhoneNumberDialogFragment extends DialogFragment {

    public OnOffButton btnYes;
    public OnOffButton btnNo;

    ButtonInterface buttonInterface;
    UiInitInterface uiInitInterface;

    Context context;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Dialog dialog = getDialog();
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadius(50);

        if (dialog.getWindow() != null) {

            WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int dialogWidth = (int) (metrics.widthPixels * 0.8);
            int dialogHeight = (int) (metrics.heightPixels * 0.8);

            lp.width = dialogWidth;
            lp.height = dialogHeight;
            dialog.getWindow().setAttributes(lp);

            dialog.getWindow().setBackgroundDrawable(shape);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.fragment_enter_phone_number_dialog);

        btnYes = dialog.findViewById(R.id.btn_yes);


        btnNo = dialog.findViewById(R.id.btn_no);


        if (uiInitInterface != null) {
            uiInitInterface.onUiInit();
        }

        return dialog;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void setButtonInterface(ButtonInterface buttonInterface) {
        this.buttonInterface = buttonInterface;
    }

    public void setUiInitInterface(UiInitInterface uiInitInterface) {
        this.uiInitInterface = uiInitInterface;
    }

    public interface ButtonInterface {
        void onYes();
        void onNo();
    }

    public interface UiInitInterface {
        void onUiInit();
    }
}
