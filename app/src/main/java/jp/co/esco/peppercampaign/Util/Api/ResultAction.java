package jp.co.esco.peppercampaign.Util.Api;

import android.util.Log;

import jp.co.esco.peppercampaign.Util.HttpTask;

/*
抽選確定API（メール送信結果API）
URL：http://52.196.247.140:8080/CMPApp/ResultAction
通信方式：POST
付随パラメタ：｛
				"campaignid":抽選用APIで返却されたcampaignid,
				"prizeid":抽選用APIで返却されたprize_id,
				"issend":0:メール送れなかったor 1:メール送信完了
				｝

レスポンス：特になし
* */

public class ResultAction {

    private final String TAG = this.getClass().getSimpleName();

    private int campaignId;
    private int prizeId;
    private boolean isSend;
    private ResultActionCallback callback;

    public ResultAction(int campaignId, int prizeId, boolean isSend, ResultActionCallback callback) {
        this.campaignId = campaignId;
        this.prizeId = prizeId;
        this.isSend = isSend;
        this.callback = callback;
    }

    public void excute() {

        final String url = "https://d2z039ds3tpmu1.cloudfront.net/CMPApp/ResultAction";
//        final String url = "http://2eedf46a.ngrok.io/CMPApp/ResultAction"; //ngrok
        // final String url = "http://172.20.10.14/Campaign/ResultAction.php"; // ローカルテスト用

        HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                "campaignid=" + String.valueOf(campaignId) + "&" +
                        "prizeid=" + String.valueOf(prizeId) + "&" +
                        "issend=" + String.valueOf(isSend ? 1 : 0)
        );

        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    callback.onFinish(response);

                } else {

                    callback.onFinish(response);
                }
            }

            @Override public void onPre() { }
        }, true).execute(request);
    }

    public interface ResultActionCallback {
        void onFinish(HttpTask.HttpResponse response);
    }
}
