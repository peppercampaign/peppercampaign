package jp.co.esco.peppercampaign;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.QiSDK;
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks;

import jp.co.esco.peppercampaign.Fragment.BeforeLotteryFragment;
import jp.co.esco.peppercampaign.Fragment.NoCampaignFragment;
import jp.co.esco.peppercampaign.Fragment.SettingFragment;
import jp.co.esco.peppercampaign.Model.Campaign;
import jp.co.esco.peppercampaign.Util.Api.GetCampaignInfo;
import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.PepperMemory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements RobotLifecycleCallbacks, ChangeFragmentInterface {

    public QiContext qiContext;
    boolean flagFragment = false;

    private int smsSendingTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            flagFragment = true;
        }

        // Register the RobotLifecycleCallbacks to this Activity.
        QiSDK.register(this, this);
    }

    @Override
    protected void onDestroy() {
        // Unregister all the RobotLifecycleCallbacks for this Activity.
        QiSDK.unregister(this);
        super.onDestroy();
    }

    private void goActivation() {

        Fragment fragment;

        // 初回起動を確認
        if (getSharedPreferences("uuid", MODE_PRIVATE).getString("uuid", "").equals("")) {

            fragment = new SettingFragment();

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();

        } else {
            PepperMemory.uuid = getSharedPreferences("uuid", MODE_PRIVATE).getString("uuid", "");
            // キャンペーンを確認
            checkCampain();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onRobotFocusGained(QiContext qiContext) {

        this.qiContext = qiContext;

        if (flagFragment) {
            flagFragment = false;
            goActivation();
        }
    }

    @Override
    public void onRobotFocusLost() {
        qiContext = null;
        Log.d(this.getClass().getSimpleName(), "onRobotFocusLost");
    }

    @Override
    public void onRobotFocusRefused(String reason) {
        Log.d(this.getClass().getSimpleName(), "onRobotFocusRefused");
    }

    @Override
    public void changeFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    public void checkCampain() {

        String uuid = PepperMemory.uuid;

        new GetCampaignInfo(uuid, new GetCampaignInfo.GetCampaignInfoCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int campaignId, String campaignName, boolean loopFlag, String resultCode) {

                // 通信成功
                if (response.isSuccess) {
                    Fragment fragment = null;
                    // リザルトコード確認
                    switch (resultCode) {
                        case GetCampaignInfo.ResultCode.NO_LOTTERY:
                        case GetCampaignInfo.ResultCode.NO_CAMPAIGN:
                            // キャンペーンなし画面
                            fragment = new NoCampaignFragment();
                            break;
                        case GetCampaignInfo.ResultCode.SUCCESS:
                            // キャンペーン画面へ遷移
                            Campaign campaign = new Campaign();
                            campaign.setId(campaignId);
                            campaign.setName(campaignName);
                            campaign.setLoopFlag(loopFlag);
                            PepperMemory.campaign = campaign;
                            fragment = new BeforeLotteryFragment();
                            break;

                    }

                    if (fragment == null) { return; }

                    changeFragment(fragment);
                }
            }
        }).excute();
    }

    public int getSmsSendingTime() {
        return smsSendingTime;
    }

    public void setSmsSendingTime(int smsSendingTime) {
        this.smsSendingTime = smsSendingTime;
    }
}
