package jp.co.esco.peppercampaign.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;

import com.aldebaran.qi.sdk.QiContext;

import jp.co.esco.peppercampaign.ChangeFragmentInterface;
import jp.co.esco.peppercampaign.MainActivity;

public class CampaignFragment extends Fragment {

    String TAG;

    // 画面遷移のためのインターフェース
    ChangeFragmentInterface fragmentInterface;

    // qiContext
    QiContext qiContext;

    // Activity
    MainActivity activity;

    // Handler
    Handler handler;

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        this.activity = (MainActivity) activity;
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MainActivity) context;
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TAG = this.getClass().getSimpleName();
        handler = new Handler();

        // 遷移元Activityから物を貰う
        fragmentInterface = activity;
        qiContext = activity.qiContext;
    }
}
