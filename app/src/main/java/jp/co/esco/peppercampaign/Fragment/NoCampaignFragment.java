package jp.co.esco.peppercampaign.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;
import jp.co.esco.peppercampaign.View.OnOffButton;

public class NoCampaignFragment extends CampaignFragment {

    OnOffButton btnNoCampaign;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_no_campaign, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //ui
        btnNoCampaign = view.findViewById(R.id.button_no_campaign);
        btnNoCampaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PepperUtil.finishApplicaton(activity);
            }
        });

        // 設定画面に行く
        view.findViewById(R.id.btn_secret_setting).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                fragmentInterface.changeFragment(new SettingFragment());
                return false;
            }
        });

        // say
        sayPepper();
    }

    private void sayPepper() {

        btnNoCampaign.changeState(false);

        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.no_campaign), new FutureInterface() {
            @Override
            public void onSuccess() {
                btnNoCampaign.changeState(true);
            }

            @Override
            public void onError(String error) {

            }
        });
    }
}
