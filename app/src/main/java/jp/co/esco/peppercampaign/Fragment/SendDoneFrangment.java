package jp.co.esco.peppercampaign.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;

public class SendDoneFrangment extends CampaignFragment {

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_send_done, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定

        // クリック時の処理を実装

        // 発話
        sayPepper();
    }

    private void sayPepper() {


        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.sms_send_success_pe), new FutureInterface() {
            @Override
            public void onSuccess() {

                // クロージング
                fragmentInterface.changeFragment(new ClosingFragment());
            }

            @Override public void onError(String error) {}
        });
    }
}
