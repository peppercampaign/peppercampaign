package jp.co.esco.peppercampaign.Util;

import android.os.Handler;

public class Timeout {

    private int limit;
    private int count;
    private TimeoutListener timeoutListener;
    private Thread timeoutThread;
    private boolean flag;

    public Timeout(int limit) {
        this.limit = limit;
    }

    public void setTimeoutListener(TimeoutListener timeoutListener) {
        this.timeoutListener = timeoutListener;
    }

    public void start() {

        if (limit == 0) {
            return;
        }

        flag = true;
        count = 0;
        final Handler handler = new Handler();

        timeoutThread = new Thread(new Runnable() {
            @Override
            public void run() {

                count++;

                if (!flag) {
                    return;
                }

                if (count > limit) {

                    // タイムアウト時
                    if (timeoutListener != null) {
                        timeoutListener.onTimeout();
                        return;
                    }
                }

                handler.postDelayed(this, 1000);
            }
        });

        timeoutThread.start();

    }

    public void resetCount() {
        count = 0;
    }

    public void stop() {
        flag = false;
    }

    public interface TimeoutListener {
        void onTimeout();
    }
}
