package jp.co.esco.peppercampaign.Fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.co.esco.peppercampaign.Model.Prize;
import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.Api.LotteryAction;
import jp.co.esco.peppercampaign.Util.PepperMemory;
import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;
import jp.co.esco.peppercampaign.View.OnOffButton;

public class BeforeLotteryFragment extends CampaignFragment {

    OnOffButton btnLottery;

    int taskCount = 0;

    SoundPool soundPool;
    int bgm;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_before_lottery, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定
        btnLottery = view.findViewById(R.id.btn_lottery);

        // クリック時の処理を実装
        btnLottery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickButton();
            }
        });


        // 設定画面に行く
        view.findViewById(R.id.btn_secret_setting).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                soundPool.stop(1);
                fragmentInterface.changeFragment(new SettingFragment());
                return false;
            }
        });

        // メール送信回数を初期化
        activity.setSmsSendingTime(0);

        soundInit();

        sayPepper();
    }

    // UUID取得
    private String getUuid() {
        return "x001";
    }

    private void sayPepper() {

        // 発話が終わるまで使用不可
        btnLottery.changeState(false);

        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.lottery_start_a) , new FutureInterface() {
            @Override
            public void onSuccess() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        btnLottery.changeState(true);
                    }
                });
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void clickButton() {

        // 発話
        sayLottery();
        // 抽選をする
        lottery();
    }

    //ボタンクリック字発話
    private void sayLottery() {

        btnLottery.changeState(false);

        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.lottery_start_2), new FutureInterface() {
            @Override
            public void onSuccess() {
                goLottery();
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    // 抽選API
    private void lottery() {

        int campaignId = PepperMemory.campaign.getId();

        new LotteryAction(campaignId, new LotteryAction.LotteryActionCallback() {

            @Override
            public void onFinish(HttpTask.HttpResponse response, String comment, String imageUrl, String imageBase64, String name, boolean isNeedPhone, int prizeId, String resultCode, String title) {

                // 通信結果確認
                if (response.isSuccess) {

                    Prize prize = new Prize();
                    prize.setId(prizeId);
                    prize.setTitle(title);
                    prize.setName(name);
                    prize.setComment(comment);
                    prize.setNeedPhone(isNeedPhone);
                    prize.setImageUrl(imageUrl);
                    prize.setImageBase64(imageBase64);

                    // リザルトコード判定
                    switch (resultCode) {
                        // 成功
                        case LotteryAction.ResultCode.SUCCESS:
                            onLotterySucceeded(prize);
                            break;

                        // 景品が無いときエラー
                        case LotteryAction.ResultCode.NO_LOTTERY:
                            fragmentInterface.changeFragment(new NoCampaignFragment());
                            break;
                    }
                }
            }
        }).excute();
    }

    // 抽選成功時
    private void onLotterySucceeded(Prize prize) {

        PepperMemory.prize = prize;

        // 画像取得
        getImage();
    }

    // 画像取得
    private void getImage() {

        final Prize prize = PepperMemory.prize;

        if (prize.getImageBase64().equals("")) {

            // SBギフトからのURL
            HttpTask.HttpRequest request = new HttpTask.HttpRequest(prize.getImageUrl(), null);
            new HttpTask(new HttpTask.HttpListener() {
                @Override
                public void onFinish(HttpTask.HttpResponse response) {

                    Bitmap image;

                    if (response.isSuccess) {
                        image = HttpTask.byte2bitmup(response.result);

                        if (image == null) {
                            if (prize.isNeedPhone()) {
                                image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.nopicture);
                            } else {
                                image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.pepper_campaign_lottery_lost);
                            }
                        }
                    }

                    else {
                        if (prize.isNeedPhone()) {
                            image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.nopicture);
                        } else {
                            image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.pepper_campaign_lottery_lost);
                        }
                    }

                    PepperMemory.prize.setImage(image);

                    goLottery();
                }

                @Override public void onPre() { }
            }, false).execute(request);

        } else {
            // 画像がBase64で渡された
            Bitmap image = HttpTask.base642bitmap(prize.getImageBase64());

            PepperMemory.prize.setImage(image);

            goLottery();
        }
    }

    // 抽選画面へ
    private void goLottery() {

        /*
        * 発話処理と、抽選処理がどちらも終わっていたら画面遷移
        * */

        taskCount ++;

        if (taskCount >= 2) {
            soundPool.stop(1);
            fragmentInterface.changeFragment(new LotteryFragment());
        }
    }

    private void soundInit() {
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                // USAGE_MEDIA
                // USAGE_GAME
                .setUsage(AudioAttributes.USAGE_GAME)
                // CONTENT_TYPE_MUSIC
                // CONTENT_TYPE_SPEECH, etc.
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();

        soundPool = new SoundPool.Builder()
                .setAudioAttributes(audioAttributes)
                // ストリーム数に応じて
                .setMaxStreams(1)
                .build();

        // one.wav をロードしておく
        bgm = soundPool.load(activity, R.raw.tyusen_bgm, 1);

        // load が終わったか確認する場合
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {

                //音楽再生
                // play(ロードしたID, 左音量, 右音量, 優先度, ループ,再生速度)
                soundPool.play(bgm, 1.0f, 1.0f, 0, Integer.MAX_VALUE, 1);
            }
        });
    }
}
