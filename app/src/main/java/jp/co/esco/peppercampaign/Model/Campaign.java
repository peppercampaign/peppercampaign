package jp.co.esco.peppercampaign.Model;

import java.io.Serializable;

public class Campaign implements Serializable {
    private int id;
    private String name;
    private boolean loop_flag;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLoopFlag(boolean loopflag) { this.loop_flag = loopflag; }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public boolean getLoopFlg() { return loop_flag; }
}
