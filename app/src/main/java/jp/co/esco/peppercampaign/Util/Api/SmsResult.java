package jp.co.esco.peppercampaign.Util.Api;

import android.util.Log;

import jp.co.esco.peppercampaign.Util.HttpTask;

public class SmsResult {

    private final String TAG = this.getClass().getSimpleName();

    private String smsId;
    private SmsResultCallback callback;

    public SmsResult(String smsId, SmsResultCallback callback) {
        this.smsId = smsId;
        this.callback = callback;
    }

    public void excute() {

        // final String url = "https://www.sms-console.jp/api5/?";
        final String url = "http://172.20.10.14/SendSms"; // ローカルテスト用

        HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                "smsid=" + smsId
        );

        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    callback.onFinish(response, true);

                } else {

                    callback.onFinish(response, false);
                }
            }

            @Override public void onPre() { }
        }, true).execute(request);
    }

    public interface SmsResultCallback {
        void onFinish(HttpTask.HttpResponse response, boolean isSuccess);
    }
}
