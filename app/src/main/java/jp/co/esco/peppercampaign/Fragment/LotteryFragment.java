package jp.co.esco.peppercampaign.Fragment;

import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.VideoView;

import jp.co.esco.peppercampaign.Model.Prize;
import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;
import jp.co.esco.peppercampaign.Util.PepperMemory;
import jp.co.esco.peppercampaign.View.AutoResizeTextView;

public class LotteryFragment extends CampaignFragment {

    Prize prize;

    VideoView videoView;
    ImageView imageBox;

    SoundPool soundPool;
    int bgm;
    int se;
    int loadedCount = 0;

    // result
    View resultArea;
    View prizeWindow;
    View solidWindow;
    ImageView imgPrize;
    AutoResizeTextView txtPrize;



    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_lottery, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // プライズ情報取得
        prize = PepperMemory.prize;

        // UI設定
        videoView = view.findViewById(R.id.video_lottery);
        imageBox = view.findViewById(R.id.image_box);
        imageBox.setVisibility(View.INVISIBLE);

        imgPrize = view.findViewById(R.id.img_prize);
        txtPrize = view.findViewById(R.id.txt_prize);
        String prizeInfo = prize.getTitle() + " : " + prize.getName();
        txtPrize.setText(prizeInfo);
        imgPrize.setImageBitmap(prize.getImage());

        prizeWindow = view.findViewById(R.id.clear_window);
        solidWindow = view.findViewById(R.id.solid);
        resultArea = view.findViewById(R.id.result_area);
        resultArea.setVisibility(View.INVISIBLE);

        // 音声設定　→　動画設定
        soundInit();
    }

    private void soundInit() {
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                // USAGE_MEDIA
                // USAGE_GAME
                .setUsage(AudioAttributes.USAGE_GAME)
                // CONTENT_TYPE_MUSIC
                // CONTENT_TYPE_SPEECH, etc.
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();

        soundPool = new SoundPool.Builder()
                .setAudioAttributes(audioAttributes)
                // ストリーム数に応じて
                .setMaxStreams(2)
                .build();

        // 音楽ファイルをロードしておく
        bgm = soundPool.load(activity, R.raw.kuji1, 1);
        if (prize.isNeedPhone()) {
            se = soundPool.load(activity, R.raw.bell_twice, 2);
        } else {
            se = soundPool.load(activity, R.raw.boo, 2);
        }

        // load が終わったか確認する場合
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loadedCount++;
                if (loadedCount == 2) {
                    startMovie();
                }
            }
        });
    }

    private void startMovie() {
        // 動画読み込み字の処理
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.start();
                // 抽選箱表示
                renderBox();
                startBgm();
                // 5秒でペッパー発話
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.lottery_decide), null);
                        // さらに5秒で結果表示
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showResult(prize.isNeedPhone());
                                // さらに5秒で動画停止
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        videoView.pause();
                                    }
                                }, 5000);
                            }
                        }, 5000);
                    }
                },5000);

            }
        });
        // 動画ロード
        if (prize.isNeedPhone()) {
            videoView.setVideoPath("android.resource://" + activity.getPackageName() + "/" + R.raw.pepper_campaign_lottery_rank_1);
        } else {
            videoView.setVideoPath("android.resource://" + activity.getPackageName() + "/" + R.raw.pepper_campaign_lottery_lose);
        }
    }

    private void startBgm() {
        //音楽再生
        // play(ロードしたID, 左音量, 右音量, 優先度, ループ,再生速度)
        //soundPool.play(bgm, 1.0f, 1.0f, 0, 1, 1);
        soundPool.play(bgm, 1.0f, 1.0f, 0, 1, 1);
    }

    // 抽選箱描画処理
    private void renderBox() {
        // フェードイン
        imageBox.setVisibility(View.VISIBLE);
        final AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(1000);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) { }

            @Override
            public void onAnimationEnd(Animation animation) {
                // フェードアウト
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AlphaAnimation animationOut = new AlphaAnimation(1.0f, 0.0f);
                        animationOut.setDuration(1000);
                        animationOut.setFillAfter(true);
                        imageBox.startAnimation(animationOut);
                    }
                }, 5000);
            }

            @Override public void onAnimationRepeat(Animation animation) { }
        });
        imageBox.startAnimation(animation);
    }


    // 結果表示処理
    private void showResult(boolean isNeedPhone) {
        if(isNeedPhone){
            resultArea.setBackgroundResource(R.drawable.prize_1);
        } else {
            resultArea.setBackgroundResource(R.drawable.pepper_campaign_lottery_lost);
            imgPrize.setVisibility(View.INVISIBLE);
            prizeWindow.setVisibility(View.INVISIBLE);
            solidWindow.setVisibility(View.INVISIBLE);
        }
        resultArea.setVisibility(View.VISIBLE);

        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(1000);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // seを鳴らす
                soundPool.play(se, 1.0f, 1.0f, 0, 0, 1);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                sayPepper();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        resultArea.startAnimation(animation);
    }

    private void sayPepper() {

        StringBuilder speechText = new StringBuilder();
        if (prize.getComment().equals("")) {

            speechText.append("\\rspd=102\\\\vct=145\\抽選結果はこちら、");
            speechText.append(prize.getTitle());
            speechText.append("、");
            speechText.append(prize.getName());
            speechText.append("です。");
            // あたり時
            if (prize.isNeedPhone()) {
                speechText.append("おめでとうございますー。");
            }

        } else {
            speechText.append(prize.getComment());
        }

        PepperUtil.pepperSayWithRundomMotion(qiContext, speechText.toString(), new FutureInterface() {
            @Override
            public void onSuccess() {

                // 5秒見せる
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        // あたりだったら電話番号入力
                        if (prize.isNeedPhone()) {
                            fragmentInterface.changeFragment(new EnterPhoneNumberFragment());
                        }

                        // ハズレだったら（クロージング）
                        else {
                            fragmentInterface.changeFragment(new ClosingFragment());
                        }
                    }
                }, 5000);
            }

            @Override public void onError(String error) { }
        });
    }
}
