package jp.co.esco.peppercampaign.Util.Api;

import android.util.Log;

import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.JsonUtil;

/*
電話番号入力チェックAPI（当選品発行API）
URL:http://52.196.247.140:8080/CMPApp/duplicateCheck
通信方式：POST
付随パラメタ：｛
					"phoneno":入力電話番号,
					"campaignid":抽選用APIで返却されたcampaignid,
					"prizeid":抽選用APIで返却されたprize_id
				｝

レスポンス
{
        "item_url": "短縮URL",
        "store_name": "CMSで登録されている店舗名"
        "result_code": "001",  ※001：正常終了　901：SBGift連携エラー（商品発注失敗）　999:重複チェックに引っかかった
        "username":"xxxxxx@xxxxxx.ne.jp",
        "password":"jfiewhogorjwjo"
}
* */

public class DuplicateCheck {

    private final String TAG = this.getClass().getSimpleName();

    private String phoneNumber;
    private int campaignId;
    private int prizeId;
    private int sendCount;
    private DuplicateCheckCallback callback;

    public DuplicateCheck(String phoneNumber, int campaignId, int prizeId, int sendcount, DuplicateCheckCallback callback) {
        this.phoneNumber = phoneNumber;
        this.campaignId = campaignId;
        this.prizeId = prizeId;
        this.sendCount = sendcount;
        this.callback = callback;
    }

    public void excute() {

         final String url = "https://d2z039ds3tpmu1.cloudfront.net/CMPApp/duplicateCheck";
//        final String url = "http://2eedf46a.ngrok.io/CMPApp/duplicateCheck";
        // final String url = "http://172.20.10.14/Campaign/DuplicateCheck.php"; // ローカルテスト用

        Log.d("url",url);
        Log.d("params",phoneNumber+","+String.valueOf(campaignId)+","+String.valueOf(prizeId)+","+sendCount);

        HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                "phoneno=" + phoneNumber + "&" +
                        "campaignid=" + String.valueOf(campaignId) + "&" +
                        "prizeid=" + String.valueOf(prizeId)+ "&" +
                        "sendcount=" + sendCount+1
        );

        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    String itemUrl = JsonUtil.getString(resultJson, "item_url");
                    String resultCode = JsonUtil.getString(resultJson, "result_code");
                    String userName = JsonUtil.getString(resultJson, "username");
                    String password = JsonUtil.getString(resultJson, "password");
                    String storeName = JsonUtil.getString(resultJson, "store_name");

                    callback.onFinish(response, itemUrl, resultCode, userName, password, storeName);

                } else {

                    callback.onFinish(response, null, null, null, null, null);
                }
            }

            @Override public void onPre() { }
        }, true).execute(request);
    }

    public static class ResultCode {
        final static public String SUCCESS = "001";
        final static public String ERROR_SBGIFT = "901";
        final static public String DUPLICATE = "999";
    }

    public interface DuplicateCheckCallback {
        void onFinish(HttpTask.HttpResponse response, String itemUrl, String resultCode, String userName, String password, String storeName);
    }
}
