package jp.co.esco.peppercampaign;

import android.support.v4.app.Fragment;

public interface ChangeFragmentInterface {
    void changeFragment(Fragment fragment);
}
