package jp.co.esco.peppercampaign.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;
import jp.co.esco.peppercampaign.View.OnOffButton;


public class SelectContractFragment extends CampaignFragment {

    OnOffButton btnContractOk;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_select_contract, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定
        btnContractOk = view.findViewById(R.id.btn_select_kiyaku_ok);

        btnContractOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentInterface.changeFragment(new EnterPhoneNumberFragment());
            }
        });

        sayPepper();
    }

    //発話、導入
    private void sayPepper() {

        btnContractOk.changeState(false);

        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.choose_a_contract), new FutureInterface() {
            @Override
            public void onSuccess() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        btnContractOk.changeState(true);
                    }
                });
            }

            @Override
            public void onError(String error) {

            }
        });
    }
}
