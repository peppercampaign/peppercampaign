package jp.co.esco.peppercampaign.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.Api.LotteryRemainCheck;
import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;
import jp.co.esco.peppercampaign.Util.PepperMemory;
import jp.co.esco.peppercampaign.Model.Campaign;

public class ClosingFragment extends CampaignFragment{

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_closing, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定

        // クリック時の処理を実装

        // 発話
        sayPepper();
    }

    private void sayPepper() {


        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.closing_pe), new FutureInterface() {
            @Override
            public void onSuccess() {

                Campaign campaign = PepperMemory.campaign;
                // ループの場合、抽選前に戻る
                if(campaign.getLoopFlg()){
                    remainCheck();
                } else {
                    // アプリ終了
                    PepperUtil.finishApplicaton(activity);
                }
            }

            @Override public void onError(String error) {}
        });
    }

    private void remainCheck(){
        int campaignId = PepperMemory.campaign.getId();

        new LotteryRemainCheck(campaignId, new LotteryRemainCheck.LotteryActionCallback() {

            @Override
            public void onFinish(HttpTask.HttpResponse response, String resultCode) {

                // 通信結果確認
                if (response.isSuccess) {

                    // リザルトコード判定
                    switch (resultCode) {
                        // 成功
                        case LotteryRemainCheck.ResultCode.SUCCESS:
                            fragmentInterface.changeFragment(new BeforeLotteryFragment());
                            break;

                        // 景品が無いときエラー
                        case LotteryRemainCheck.ResultCode.NO_LOTTERY:
                            fragmentInterface.changeFragment(new NoCampaignFragment());
                            break;
                    }
                }
            }
        }).excute();

    }
}
