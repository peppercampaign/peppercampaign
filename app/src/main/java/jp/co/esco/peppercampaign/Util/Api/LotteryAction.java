package jp.co.esco.peppercampaign.Util.Api;

import android.util.Log;

import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.JsonUtil;


/*
*キャンペーン抽選用API（抽選ボタン押下時API）
URL：http://52.196.247.140:8080/CMPApp/LotteryAction
通信方式：POST
付随パラメタ：{ "cmpid":キャンペーン有無確認API }

レスポンス
{
        "comment": "またチャレンジしてね",
        "img_base64":"ivJ(fewo～",    ※ 画像Base64バイナリ
        "img_url": "null",            ※ 画像URL
        "name": "残念。はずれです。",
        "need_phone": 0,  ※0：電話番号入力なし　1:電話番号入力あり
        "prize_id": 66,
        "result_code": "001", ※001：正常終了, 901:全部抽選済み
        "title": "ハズレ"
}
* */

public class LotteryAction {

    private final String TAG = this.getClass().getSimpleName();

    private int campaignId;
    private LotteryActionCallback callback;

    public LotteryAction(int campaignId, LotteryActionCallback callback) {
        this.campaignId = campaignId;
        this.callback = callback;
    }

    public void excute() {

        final String url = "https://d2z039ds3tpmu1.cloudfront.net/CMPApp/LotteryAction";
//        final String url = "http://2eedf46a.ngrok.io/CMPApp/LotteryAction"; //ngrok
        // final String url = "http://172.20.10.14/Campaign/LotteryAction.php"; // ローカルテスト用

        HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                "cmpid=" + campaignId
        );

        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    String comment = JsonUtil.getString(resultJson, "comment");
                    String imageUrl = JsonUtil.getString(resultJson, "image_url");
                    String imageBase64 = JsonUtil.getString(resultJson, "image_base64");
                    String name = JsonUtil.getString(resultJson, "name");
                    boolean isNeedPhone = JsonUtil.getInt(resultJson, "need_phone") == 1;
                    int prizeId = JsonUtil.getInt(resultJson, "prize_id");
                    String resultCode = JsonUtil.getString(resultJson, "result_code");
                    String title = JsonUtil.getString(resultJson, "title");

                    callback.onFinish(response, comment, imageUrl, imageBase64, name, isNeedPhone, prizeId, resultCode, title);

                } else {

                    callback.onFinish(response, "", "", "", "", false, -1, "", "");
                }
            }

            @Override public void onPre() { }
        }, true).execute(request);
    }

    public static class ResultCode{
        final static public String SUCCESS = "001";
        final static public String NO_LOTTERY = "901";
    }


    public interface LotteryActionCallback {
        void onFinish(HttpTask.HttpResponse response,
                      String comment,
                      String imageUrl,
                      String imageBase64,
                      String name,
                      boolean isNeedPhone,
                      int prizeId,
                      String resultCode,
                      String title);
    }
}
