package jp.co.esco.peppercampaign.Util.Pepper;

public interface FutureInterface {
    void onSuccess();
    void onError(String error);
}
