package jp.co.esco.peppercampaign.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.View.AutoResizeTextView;


public class ContractDialogFragment extends DialogFragment {

    String title = "";
    String text = "";

    AutoResizeTextView txtTitle;
    TextView txtText;

    Context context;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Dialog dialog = getDialog();

        if (dialog.getWindow() != null) {

            WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            int dialogWidth = (int) (metrics.widthPixels * 0.8);
            int dialogHeight = (int) (metrics.heightPixels * 0.8);

            lp.width = dialogWidth;
            lp.height = dialogHeight;
            dialog.getWindow().setAttributes(lp);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.fragment_contract_dialog);

        dialog.findViewById(R.id.btn_contract_finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        txtTitle = dialog.findViewById(R.id.txt_contract_title);
        txtTitle.setText(this.title);

        txtText = dialog.findViewById(R.id.txt_contract_text);
        txtText.setText(this.text);

        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }
}
