package jp.co.esco.peppercampaign.Util.Api;

import android.util.Log;

import jp.co.esco.peppercampaign.Util.HttpTask;

public class SendSms {

    private final String TAG = this.getClass().getSimpleName();

    private String userName;
    private String password;
    private String phoneNumber;
    private String smsText;
    private String smsId;
    private SendSmsCallback callback;

    public SendSms(String userName, String password, String phoneNumber, String smsText, SendSmsCallback callback) {
        this.userName = userName;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.smsText = smsText;
        this.smsId = createSmsId();
        this.callback = callback;
    }

    public void excute() {

        final String url = "https://www.sms-console.jp/api/?";
        // final String url = "http://172.20.10.14/Campaign/SendSms.php"; // ローカルテスト用

        HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                "username=" + userName + "&" +
                        "password=" + password + "&" +
                        "mobilenumber=" + phoneNumber + "&" +
                        "smstext=" + smsText + "&" +
                        "smsid=" + smsId + "&" +
                        "status=1"
        );

        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    callback.onFinish(response, smsId);

                } else {

                    callback.onFinish(response, null);
                }
            }

            @Override public void onPre() { }
        }, true).execute(request);
    }

    // smsIdを作成
    private String createSmsId() {
        long time =  System.currentTimeMillis();
        String strTime = String.valueOf(time);
        if (strTime.length() > 20) {
            strTime = strTime.substring(strTime.length() - 20);
        }
        return strTime;
    }

    public interface SendSmsCallback {
        void onFinish(HttpTask.HttpResponse response, String smsId);
    }
}
