package jp.co.esco.peppercampaign.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.co.esco.peppercampaign.Model.Campaign;
import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;
import jp.co.esco.peppercampaign.View.CampaignAdapter;

public class CampaignListFragment extends CampaignFragment {

    // キャンペーンリスト
    ListView lstCampaign;
    CampaignAdapter adapter;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_campaign_list, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定
        lstCampaign = view.findViewById(R.id.lst_campaign);

        // キャンペーンリスト取得API
        getCampaignList();
    }

    // キャンペーンリスト取得
    private void getCampaignList() {

        final String url = "aaa";

        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                "datloginid=pepper.echo.pepper@gmail.com");

        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {
                    String result = HttpTask.byte2string(response.result);
                    result = "{\"list\":" + result + "}";
                    Log.d(TAG, result);
                    // JSON読解
                    readJsonGetCampaignList(result);
                }
            }

            @Override public void onPre() { }
        }, true).execute(request);
    }

    private void readJsonGetCampaignList(String strJson) {

        ArrayList<Campaign> campaigns = new ArrayList<>();

        try {
            JSONObject rootJson = new JSONObject(strJson);
            JSONArray campaignsJson = rootJson.getJSONArray("list");
            for (int i = 0; i < campaignsJson.length(); i++) {
                Campaign campaign = new Campaign();
                JSONObject campaignJson = campaignsJson.getJSONObject(i);
                campaign.setId(campaignJson.getInt("campaign_id"));
                campaign.setName(campaignJson.getString("campaign_name"));
                campaigns.add(campaign);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        renderList(campaigns);

        sayPepper();
    }

    private void renderList(ArrayList<Campaign> campaigns) {

        adapter = new CampaignAdapter(activity);
        adapter.setCampaignList(campaigns);
        lstCampaign.setAdapter(adapter);
    }

    private void sayPepper() {
        adapter.setButtonEnable(false);

        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.select_campaign), new FutureInterface() {
            @Override
            public void onSuccess() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.setButtonEnable(true);
                    }
                });
            }

            @Override
            public void onError(String error) {

            }
        });
    }
}
