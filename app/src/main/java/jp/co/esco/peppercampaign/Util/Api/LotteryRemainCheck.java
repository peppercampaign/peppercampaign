package jp.co.esco.peppercampaign.Util.Api;

import android.util.Log;

import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.JsonUtil;


/*
*抽選可否API
URL：http://52.196.247.140:8080/CMPApp/Lottery
通信方式：POST
付随パラメタ：{ "cmpid":キャンペーン有無確認API }

レスポンス
{
        "result_code": "001", ※001：正常終了, 901:全部抽選済み
}
* */

public class LotteryRemainCheck {

    private final String TAG = this.getClass().getSimpleName();

    private int campaignId;
    private LotteryActionCallback callback;

    public LotteryRemainCheck(int campaignId, LotteryActionCallback callback) {
        this.campaignId = campaignId;
        this.callback = callback;
    }

    public void excute() {

        final String url = "https://d2z039ds3tpmu1.cloudfront.net/CMPApp/LotteryRemainCheck";
//        final String url = "http://2eedf46a.ngrok.io/CMPApp/LotteryRemainCheck"; //ngrok
        // final String url = "http://172.20.10.14/Campaign/LotteryRemainCheck.php"; // ローカルテスト用

        HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                "cmpid=" + campaignId
        );

        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    String resultCode = JsonUtil.getString(resultJson, "result_code");

                    callback.onFinish(response, resultCode);

                } else {

                    callback.onFinish(response,  "");
                }
            }

            @Override public void onPre() { }
        }, true).execute(request);
    }

    public static class ResultCode{
        final static public String SUCCESS = "001";
        final static public String NO_LOTTERY = "901";
    }


    public interface LotteryActionCallback {
        void onFinish(HttpTask.HttpResponse response, String resultCode);
    }
}
