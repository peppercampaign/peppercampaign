package jp.co.esco.peppercampaign.Util;

import jp.co.esco.peppercampaign.Model.CMSUserInfo;
import jp.co.esco.peppercampaign.Model.Campaign;
import jp.co.esco.peppercampaign.Model.Prize;

public class PepperMemory {
    public static String uuid;
    public static Prize prize;
    public static Campaign campaign;
    public static CMSUserInfo userInfo;
}
