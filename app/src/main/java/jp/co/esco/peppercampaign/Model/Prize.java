package jp.co.esco.peppercampaign.Model;

import android.graphics.Bitmap;

import java.io.Serializable;

public class Prize implements Serializable {
    private int id;
    private String title;
    private String name;
    private String imageUrl;
    private String imageBase64;
    private String comment;
    private boolean needPhone;
    private String prizeUrl;
    private Bitmap image;
    private String storeName;

    public void setName(String name) {
        this.name = name;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setNeedPhone(boolean needPhone) {
        this.needPhone = needPhone;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPrizeUrl(String prizeUrl) {
        this.prizeUrl = prizeUrl;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public String getTitle() {
        return title;
    }

    public String getComment() {
        return comment;
    }

    public boolean isNeedPhone() {
        return needPhone;
    }

    public int getId() {
        return id;
    }

    public String getPrizeUrl() {
        return prizeUrl;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getStoreName() {
        return storeName;
    }
}
