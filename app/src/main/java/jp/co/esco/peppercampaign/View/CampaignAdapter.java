package jp.co.esco.peppercampaign.View;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import jp.co.esco.peppercampaign.Fragment.BeforeLotteryFragment;
import jp.co.esco.peppercampaign.MainActivity;
import jp.co.esco.peppercampaign.Model.Campaign;
import jp.co.esco.peppercampaign.R;

public class CampaignAdapter extends BaseAdapter {

    private ArrayList<Campaign> campaigns;
    private Context context;
    private LayoutInflater layoutInflater;

    private boolean isButtonEnable = true;

    public CampaignAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setCampaignList(ArrayList<Campaign> campaignList) {
        campaignList.add(0, new Campaign());
        this.campaigns = campaignList;
    }

    public void setButtonEnable(boolean buttonEnable) {
        isButtonEnable = buttonEnable;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return campaigns.size();
    }

    @Override
    public Object getItem(int i) {
        return campaigns.get(i);
    }

    @Override
    public long getItemId(int i) {
        return campaigns.get(i).getId();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        view = layoutInflater.inflate(R.layout.item_campaign, null);

        final Activity activity = (Activity) context;
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        Log.d("size", point.toString());

        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, point.y / 6);
        view.setLayoutParams(params);

        TextView txtId = view.findViewById(R.id.campaign_item_id);
        TextView txtName = view.findViewById(R.id.campaign_item_name);
        OnOffButton btnDone = view.findViewById(R.id.campaign_item_button);
        btnDone.changeState(isButtonEnable);

        // 先頭だけ変化させる（ヘッダー）
        if (i == 0) {
            txtId.setText("No");
            txtId.setTextColor(context.getColor(R.color.white));
            txtName.setText("キャンペーン名");
            txtName.setTextColor(context.getColor(R.color.white));
            btnDone.setVisibility(View.GONE);
            view.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            txtId.setText(String.valueOf(campaigns.get(i).getId()));
            txtName.setText(campaigns.get(i).getName());
            view.setBackgroundColor(context.getColor(R.color.white));
            if (activity instanceof MainActivity) {
                btnDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((MainActivity) activity).changeFragment(new BeforeLotteryFragment());
                    }
                });
            }
        }

        return view;
    }
}
