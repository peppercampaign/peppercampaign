package jp.co.esco.peppercampaign.Fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import jp.co.esco.peppercampaign.Model.Prize;
import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.PepperMemory;
import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;
import jp.co.esco.peppercampaign.View.AutoResizeTextView;

public class LotteryResultFragment extends CampaignFragment {

    ImageView imgPrize;
    AutoResizeTextView txtPrize;

    Prize prize;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_lottery_result, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // プライズ情報取得
        prize = PepperMemory.prize;

        // UI設定
        imgPrize = view.findViewById(R.id.img_prize);
        txtPrize = view.findViewById(R.id.txt_prize);
        String prizeInfo = prize.getTitle() + " : " + prize.getName();
        txtPrize.setText(prizeInfo);
        imgPrize.setImageBitmap(prize.getImage());

        // クリック時の処理を実装

        // 発話
        sayPepper();
    }

    private void sayPepper() {

        StringBuilder speechText = new StringBuilder();
        if (prize.getComment().equals("")) {

            speechText.append("抽選結果はこちら、");
            speechText.append(prize.getTitle());
            speechText.append("、");
            speechText.append(prize.getName());
            speechText.append("です。");
            // あたり時
            if (prize.isNeedPhone()) {
                speechText.append("おめでとうございますー。");
            }

        } else {
            speechText.append(prize.getComment());
        }

        PepperUtil.pepperSayWithRundomMotion(qiContext, speechText.toString(), new FutureInterface() {
            @Override
            public void onSuccess() {

                // 5秒見せる
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        // あたりだったら電話番号入力
                        if (prize.isNeedPhone()) {
                            fragmentInterface.changeFragment(new EnterPhoneNumberFragment());
                        }

                        // ハズレだったら（どうする？）
                        else {
                            PepperUtil.pepperSay(qiContext, "アプリを終了します", new FutureInterface() {
                                @Override
                                public void onSuccess() {
                                    PepperUtil.finishApplicaton(activity);
                                }

                                @Override public void onError(String error) { }
                            });
                        }
                    }
                }, 5000);
            }

            @Override public void onError(String error) { }
        });
    }
}
