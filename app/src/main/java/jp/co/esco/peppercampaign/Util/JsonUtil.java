package jp.co.esco.peppercampaign.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil {

    public static String getString(String json, String key) {
        String value = "";
        try {
            JSONObject jsonObject = new JSONObject(json);
            value = jsonObject.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }

    public static int getInt(String json, String key) {
        int value = -1;
        try {
            JSONObject jsonObject = new JSONObject(json);
            value = jsonObject.getInt(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return value;
    }
}
