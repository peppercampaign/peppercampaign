package jp.co.esco.peppercampaign.Util.Api;

import android.util.Log;

import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.JsonUtil;

/*
* キャンペーン有無確認API
URL：http://52.196.247.140:8080/CMPApp/GetCampaignInfo
通信方式：POST
付随パラメタ：{ "uuid":"機体uuid(PoC時は設定画面のuuid)" }

レスポンス：
{
        "campaignid": 2,
        "campaign_name":"テストキャンペーン"
        "result_code": "001", ※001：正常終了, 901:全部抽選済み, 999:利用できるキャンペーンがない
}
* */

public class GetCampaignInfo {

    private final String TAG = this.getClass().getSimpleName();

    private String uuid;
    private GetCampaignInfoCallback callback;

    public GetCampaignInfo(String uuid, GetCampaignInfoCallback callback) {
        this.uuid = uuid;
        this.callback = callback;
    }

    public void excute() {

        final String url = "https://d2z039ds3tpmu1.cloudfront.net/CMPApp/GetCampaignInfo"; //PPD環境
//        final String url = "http://2eedf46a.ngrok.io/CMPApp/GetCampaignInfo";
        // final String url = "http://172.20.10.14/Campaign/GetCampaignInfo.php"; // ローカルテスト用

        HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                "uuid=" + uuid
        );

        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    int campaignId = JsonUtil.getInt(resultJson, "campaignid");
                    String campaignName = JsonUtil.getString(resultJson, "campaign_name");
                    String resultCode = JsonUtil.getString(resultJson, "result_code");
                    boolean loopFlag = JsonUtil.getInt(resultJson, "loop_flg") == 1;

                    callback.onFinish(response, campaignId, campaignName, loopFlag, resultCode);

                } else {

                    callback.onFinish(response, -1, null, false, null);
                }
            }

            @Override public void onPre() { }
        }, true).execute(request);
    }

    public static class ResultCode {
        final public static String SUCCESS = "001";
        final public static String NO_LOTTERY = "901";
        final public static String NO_CAMPAIGN = "999";
    }

    public interface GetCampaignInfoCallback {
        void onFinish(HttpTask.HttpResponse response, int campaignId, String campaignName, boolean loopFlag, String resultCode);
    }
}
