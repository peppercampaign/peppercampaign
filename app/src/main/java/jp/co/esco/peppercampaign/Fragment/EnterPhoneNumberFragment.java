package jp.co.esco.peppercampaign.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.sql.Time;

import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.Api.ResultAction;
import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;
import jp.co.esco.peppercampaign.Util.PepperMemory;
import jp.co.esco.peppercampaign.Util.Timeout;
import jp.co.esco.peppercampaign.View.AutoResizeTextView;
import jp.co.esco.peppercampaign.View.OnOffButton;

public class EnterPhoneNumberFragment extends CampaignFragment {

    AutoResizeTextView txtPhoneNumber;

    OnOffButton btn1;
    OnOffButton btn2;
    OnOffButton btn3;
    OnOffButton btn4;
    OnOffButton btn5;
    OnOffButton btn6;
    OnOffButton btn7;
    OnOffButton btn8;
    OnOffButton btn9;
    OnOffButton btn0;

    OnOffButton btnBack;
    OnOffButton btnDone;
    OnOffButton btnContract;
    OnOffButton btnFinish;

    ImageView imgNumberMisstake;

    Timeout timeout;

    //入力されている電話番号
    String phoneNumber = "";

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_enter_phone_number, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // button init
        btn0 = view.findViewById(R.id.marketing_button_0);
        btn1 = view.findViewById(R.id.marketing_button_1);
        btn2 = view.findViewById(R.id.marketing_button_2);
        btn3 = view.findViewById(R.id.marketing_button_3);
        btn4 = view.findViewById(R.id.marketing_button_4);
        btn5 = view.findViewById(R.id.marketing_button_5);
        btn6 = view.findViewById(R.id.marketing_button_6);
        btn7 = view.findViewById(R.id.marketing_button_7);
        btn8 = view.findViewById(R.id.marketing_button_8);
        btn9 = view.findViewById(R.id.marketing_button_9);
        btnBack = view.findViewById(R.id.marketing_button_back);
        btnDone = view.findViewById(R.id.marketing_button_done);
        btnContract = view.findViewById(R.id.kiyaku);
        btnFinish = view.findViewById(R.id.btn_finish);
        // set cliclListeneer
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("0");
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("1");
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("2");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("6");
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("7");
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("8");
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("9");
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickDone();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("");
            }
        });
        btnContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickContract();
            }
        });
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickFinish();
            }
        });
        // init active
        btnBack.changeState(false);
        btnDone.changeState(false);
        // 最初の発話が終わるまでクリック不可
        btnDone.setEnabled(false);
        btnContract.changeState(false);

        // その他UI
        txtPhoneNumber = view.findViewById(R.id.txt_phone_number);
        txtPhoneNumber.setText("");
        imgNumberMisstake = view.findViewById(R.id.img_number_misstake);
        imgNumberMisstake.setVisibility(View.GONE);
        changeButtonsState(true);



        // 発話
        sayGreeting();
    }

    @Override
    public void onStop() {
        if (timeout != null) {
            timeout.stop();
        }
        super.onStop();
    }

    //
    private void initTimeout() {
        timeout = new Timeout(15);
        timeout.setTimeoutListener(new Timeout.TimeoutListener() {
            @Override
            public void onTimeout() {
                // ダイアログ表示
                timeout.stop();
                timeout.setTimeoutListener(null);
                final EnterPhoneNumberDialogFragment dialog = new EnterPhoneNumberDialogFragment();
                dialog.setCancelable(false);
                dialog.setUiInitInterface(new EnterPhoneNumberDialogFragment.UiInitInterface() {
                    @Override
                    public void onUiInit() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                dialog.btnNo.changeState(false);
                                dialog.btnYes.changeState(false);
                            }
                        });
                        dialog.btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                timeout.stop();
                                dialog.dismiss();
                                initTimeout();
                            }
                        });
                        dialog.btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                timeout.stop();
                                dialog.dismiss();
                                sendActionResult();
                            }
                        });
                    }
                });
                dialog.show(getFragmentManager(), TAG);
                // 発話
                PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.timeout_1), new FutureInterface() {
                    @Override
                    public void onSuccess() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                dialog.btnNo.changeState(true);
                                dialog.btnYes.changeState(true);
                                // 次のタイムアウト開始
                                timeout = new Timeout(10);
                                timeout.setTimeoutListener(new Timeout.TimeoutListener() {
                                    @Override
                                    public void onTimeout() {
                                        sendActionResult();
                                        dialog.dismiss();
                                    }
                                });
                                timeout.start();
                            }
                        });
                    }

                    @Override
                    public void onError(String error) {

                    }
                });

            }
        });
        timeout.start();
    }

    // 発話
    private void sayGreeting() {
        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.please_enter_phone_number), new FutureInterface() {

            @Override
            public void onSuccess() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        // 発話後にボタンを使えるようにする（次へボタンは活性、非活性が変化するわけではない）
                        btnDone.setEnabled(true);
                        btnContract.changeState(true);

                        // タイムアウト設定
                        initTimeout();
                    }
                });
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    // 規約ボタンクリック時
    private void clickContract() {

        // タイムアウトリセット
        if (timeout != null) {
            timeout.resetCount();
        }

        // 発話前にボタンをクリック不可にする
        btnDone.setEnabled(false);
        btnContract.changeState(false);

        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.click_contract), new FutureInterface() {
            @Override
            public void onSuccess() {
                // 規約画面へ遷移
                fragmentInterface.changeFragment(new SelectContractFragment());
            }

            @Override public void onError(String error) { }
        });
    }

    // 次へボタンクリック時
    private void clickDone() {

        // タイムアウトリセット
        if (timeout != null) {
            timeout.resetCount();
        }

        // 桁数チェック
        if (phoneNumber.length() != 11) {

            // 発話前にボタンをクリック不可にする
            btnDone.setEnabled(false);
            btnContract.changeState(false);

            PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.phone_number_not_11), new FutureInterface() {

                @Override
                public void onSuccess() {
                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // 発話後にボタンを使えるようにする（次へボタンは活性、非活性が変化するわけではない）
                            btnDone.setEnabled(true);
                            btnContract.changeState(true);
                        }
                    });
                }

                @Override public void onError(String error) { }
            });
        } else {
            // 電話番号形式チェック
            if (!phoneNumber.substring(0,3).equals("070") &&
                    !phoneNumber.substring(0,3).equals("080") &&
                    !phoneNumber.substring(0,3).equals("090")) {
                // 電話番号間違い
                fragmentInterface.changeFragment(new MisstakePhoneNumberFragment());
            } else {
                // sms送信, 必要な情報を添えて
                Bundle bundle = new Bundle();
                bundle.putString("phone_number", phoneNumber);
                SendSmsFragment fragment = new SendSmsFragment();
                fragment.setArguments(bundle);

                fragmentInterface.changeFragment(fragment);
            }
        }
    }

    // 携帯番号更新
    private void enterPhoneNumber(String number) {

        // タイムアウトリセット
        if (timeout != null) {
            timeout.resetCount();
        }

        // 1文字削除
        if (number.equals("")) {

            // 消すので最低1文字は欲しい
            if (phoneNumber.length() < 1) {
                return;
            }

            // 一文字削除（文字列切り抜き）
            phoneNumber = phoneNumber.substring(0, phoneNumber.length() - 1);
        }

        else {

            // 携帯電話番号は11桁
            if (phoneNumber.length() >= 11) {
                return;
            }

            phoneNumber += number;
        }

        // ****で表示する
        StringBuilder secret = new StringBuilder();
        for (int i = 0; i < phoneNumber.length(); i++) {
            secret.append("*");
        }

        // 文字数チェック
        // 11文字なら数字を非活性, 次へを活性化
        if (phoneNumber.length() >= 11) {
            changeButtonsState(false);
            btnDone.changeActiveState(true);
        } else {
            changeButtonsState(true);
            btnDone.changeActiveState(false);
        }
        // 1文字も無ければバックを非活性
        if (phoneNumber.length() <= 0) {
            btnBack.changeState(false);
        } else {
            btnBack.changeState(true);
        }

        // 電話番号入力チェック
        // 3文字になった時（だけでいいのか。。。）
        if (!number.equals("")) {
            if (phoneNumber.length() == 3) {
                // 先頭3文字をチェック
                String top = phoneNumber.substring(0, 3);
                if (top.equals("090") || top.equals("080") || top.equals("070")) {
                    Log.d(TAG, "phone number ok");
                } else {
                    showErrorPhoneNumber();
                }
            }
        }

        // *** を表示
        txtPhoneNumber.setText(secret.toString());
    }

    // 電話番号エラーを出す
    private void showErrorPhoneNumber() {
        imgNumberMisstake.setVisibility(View.VISIBLE);
        // 5秒で消す
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                imgNumberMisstake.setVisibility(View.GONE);
            }
        },5000);

        // 発話前にボタンをクリック不可にする
        btnDone.setEnabled(false);
        btnContract.changeState(false);

        // 発話
        PepperUtil.pepperSay(qiContext, getString(R.string.not_phone_number), new FutureInterface() {
            @Override
            public void onSuccess() {

                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        // 発話後にボタンを使えるようにする（次へボタンは活性、非活性が変化するわけではない）
                        btnDone.setEnabled(true);
                        btnContract.changeState(true);
                    }
                });
            }

            @Override public void onError(String error) { }
        });
    }

    // ボタン状態変化
    private void changeButtonsState(boolean isActive) {
        btn0.changeState(isActive);
        btn1.changeState(isActive);
        btn2.changeState(isActive);
        btn3.changeState(isActive);
        btn4.changeState(isActive);
        btn5.changeState(isActive);
        btn6.changeState(isActive);
        btn7.changeState(isActive);
        btn8.changeState(isActive);
        btn9.changeState(isActive);
    }

    // 終了ボタンクリック
    private void clickFinish() {

        if (timeout != null) {
            timeout.stop();
        }

        // 発話
        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.timeout_1), new FutureInterface() {
            @Override
            public void onSuccess() {
                final EnterPhoneNumberDialogFragment dialog = new EnterPhoneNumberDialogFragment();
                dialog.setCancelable(false);
                dialog.setUiInitInterface(new EnterPhoneNumberDialogFragment.UiInitInterface() {
                    @Override
                    public void onUiInit() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                dialog.btnNo.changeState(true);
                                dialog.btnYes.changeState(true);
                            }
                        });
                        dialog.btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                timeout.stop();
                                dialog.btnNo.setBackgroundResource(R.drawable.radius_button_gray);
                                dialog.dismiss();
                                initTimeout();
                            }
                        });
                        dialog.btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                timeout.stop();
                                dialog.btnYes.setBackgroundResource(R.drawable.radius_button_gray);
                                dialog.dismiss();
                                sendActionResult();
                            }
                        });
                    }
                });
                dialog.show(getFragmentManager(), TAG);
            }

            @Override
            public void onError(String error) {

            }
        });

    }

    private void sendActionResult() {
        int campaignId = PepperMemory.campaign.getId();
        int prizeId = PepperMemory.prize.getId();
        boolean isSend = false;
        new ResultAction(campaignId, prizeId, isSend, new ResultAction.ResultActionCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {
                // クロージング
                fragmentInterface.changeFragment(new ClosingFragment());
//                PepperUtil.finishApplicaton(activity);
            }
        }).excute();
    }
}
