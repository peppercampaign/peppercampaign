package jp.co.esco.peppercampaign.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import jp.co.esco.peppercampaign.Model.Campaign;
import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.Api.GetCampaignInfo;
import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.PepperMemory;

public class SettingFragment extends CampaignFragment {

    EditText txtUuid;
    Button btnSave;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定
        txtUuid = view.findViewById(R.id.txt_uuid);
        btnSave = view.findViewById(R.id.btn_save);

        // クリック時の処理を実装
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickBtnSave();
            }
        });

        //preferenceを確認
        SharedPreferences preferences = activity.getSharedPreferences("uuid", Context.MODE_PRIVATE);
        String uuid = preferences.getString("uuid", "");
        txtUuid.setText(uuid);
    }

    private void onClickBtnSave() {

        // データを保存
        String uuid = txtUuid.getText().toString();
        SharedPreferences preferences = activity.getSharedPreferences("uuid", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("uuid", uuid);
        editor.apply();

        PepperMemory.uuid = uuid;

        activity.checkCampain();
    }
}
