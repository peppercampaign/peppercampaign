package jp.co.esco.peppercampaign.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.co.esco.peppercampaign.Model.CMSUserInfo;
import jp.co.esco.peppercampaign.Model.Campaign;
import jp.co.esco.peppercampaign.Model.Prize;
import jp.co.esco.peppercampaign.R;
import jp.co.esco.peppercampaign.Util.Api.DuplicateCheck;
import jp.co.esco.peppercampaign.Util.Api.ResultAction;
import jp.co.esco.peppercampaign.Util.Api.SendSms;
import jp.co.esco.peppercampaign.Util.Api.SmsResult;
import jp.co.esco.peppercampaign.Util.PepperMemory;
import jp.co.esco.peppercampaign.Util.HttpTask;
import jp.co.esco.peppercampaign.Util.Pepper.FutureInterface;
import jp.co.esco.peppercampaign.Util.Pepper.PepperUtil;

public class SendSmsFragment extends CampaignFragment {

    String phoneNumber;
    String smsId;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_send_sms, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // fragmentの値渡し受け取り
        Bundle bundle = getArguments();
        if (bundle != null) {
            phoneNumber = bundle.getString("phone_number");
        }

        // UI設定

        // クリック時の処理を実装

        // 発話
        sayPepper();
    }

    private void sayPepper() {

        PepperUtil.pepperSayWithRundomMotion(qiContext, getString(R.string.send_sms), new FutureInterface() {
            @Override
            public void onSuccess() {

                // 強制メール送信チェック用(テスト時は重複チェックをコメントアウトすること）
//                testSendMail();

                // 重複チェック
                checkOverlap();
            }

            @Override public void onError(String error) {}
        });
    }

    // 重複チェック
    private void checkOverlap() {

        int campaignId = PepperMemory.campaign.getId();
        int prizeId = PepperMemory.prize.getId();

        new DuplicateCheck(phoneNumber, campaignId, prizeId, activity.getSmsSendingTime(), new DuplicateCheck.DuplicateCheckCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, String itemUrl, String resultCode, String userName, String password, String storeName) {
                if (response.isSuccess) {
                    switch (resultCode) {
                        // 重複なし
                        case DuplicateCheck.ResultCode.SUCCESS:

                            CMSUserInfo userInfo = new CMSUserInfo();
                            userInfo.setId(userName);
                            userInfo.setPassword(password);

                            PepperMemory.userInfo = userInfo;
                            PepperMemory.prize.setPrizeUrl(itemUrl);
                            PepperMemory.prize.setStoreName(storeName);

                            sendSms();
                            break;

                        // 重複あり
                        case DuplicateCheck.ResultCode.DUPLICATE:
                            resultAction(false); // 景品受け取り失敗を伝える
                            onDuplication();
                            break;

                        // SBgiftエラー
                        case DuplicateCheck.ResultCode.ERROR_SBGIFT:
                            resultAction(false); // 景品受け取り失敗を伝える
                            onGiftError();
                            break;

                        default:
                            resultAction(false);
                            onGiftError();
                            break;
                    }
                } else {
                    resultAction(false); // 景品受け取り失敗を伝える
                    onGiftError();
                }
            }
        }).excute();
    }

    // sms送信処理
    private void sendSms() {

        CMSUserInfo userInfo = PepperMemory.userInfo;
        String smsText = getString(R.string.sms_message) + "\n" + PepperMemory.prize.getStoreName() + "\n" + PepperMemory.prize.getPrizeUrl();
        Log.d(TAG, "文字数：" + smsText.length());

        // 送信回数を1回更新
        activity.setSmsSendingTime(activity.getSmsSendingTime() + 1);

        new SendSms(userInfo.getId(), userInfo.getPassword(), phoneNumber, smsText, new SendSms.SendSmsCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, String smsId_) {

                if (response.isSuccess) {

                    resultAction(true); // 景品受け取り成功を伝える
                    // 送信成功画面へ
                    fragmentInterface.changeFragment(new SendDoneFrangment());

                } else {
                    smsSendFail();
                }
            }
        }).excute();
    }

    private void smsSendFail() {

        if (activity.getSmsSendingTime() < 4) {
            // 電話番号再入力へ
            fragmentInterface.changeFragment(new SendErrorFragment());
        } else {
            resultAction(false); // 景品受け取り失敗を伝える
            // エラー画面へ
            fragmentInterface.changeFragment(new SendErrorFinishFragment());
        }
    }

    // 電話番号重複時
    private void onDuplication() {
        PepperUtil.pepperSay(qiContext, getString(R.string.duplicate_phone_number), new FutureInterface() {
            @Override
            public void onSuccess() {

                fragmentInterface.changeFragment(new ClosingFragment());

            }

            @Override public void onError(String error) { }
        });
    }

    // SBギフトエラー時
    private void onGiftError() {
        fragmentInterface.changeFragment(new SbgiftErrorFragment());
    }

    // 景品受け取り結果送信
    private void resultAction(boolean b) {

        Campaign campaign = PepperMemory.campaign;
        Prize prize = PepperMemory.prize;

        new ResultAction(campaign.getId(), prize.getId(), b, new ResultAction.ResultActionCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

            }
        }).excute();
    }

    private void testSendMail(){
        CMSUserInfo userInfo = new CMSUserInfo();
        userInfo.setId("SBRobo0");
        userInfo.setPassword("SBRobo0Robo0");

        PepperMemory.userInfo = userInfo;
        PepperMemory.prize.setPrizeUrl("test");
        PepperMemory.prize.setStoreName("強制メールテスト");

        sendSms();
    }
}
